package geo

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "os"
    "strings"
)

type Directions struct {
    Type     string
    Features []struct {
        Bbox       []float32
        Type       string
        Properties struct {
            Segments []struct {
                Distance float32
                Duration float32
                Steps    []struct {
                    Distance    float32
                    Duration    float32
                    Type        int
                    Instruction string
                    Name        string
                    Way_points  []int
                }
            }
            Summary struct {
                Distance float32
                Duration float32
            }
        }
    }
}

type Geocode struct {
    Geocoding []interface{}
    Type      string
    Features  []struct {
        Type     string
        Geometry struct {
            Type        string
            Coordinates []float32
        }
    }
}

func wrap(param *string) string {
    return `"` + *param + `"`
}

func GetPoint(point string, geo_url string) string {
    client := &http.Client{}

    s := strings.ReplaceAll(point, " ", "%20")
    url := geo_url + s

    req, _ := http.NewRequest("GET", url, nil)
    req.Header.Add("Accept", "application/geo+json; charset=utf-8")

    resp, err := client.Do(req)
    if err != nil {
        fmt.Println("Error sending geocode request")
        os.Exit(1)
    }
    defer resp.Body.Close()

    var g Geocode
    resp_body, _ := ioutil.ReadAll(resp.Body)
    json.Unmarshal(resp_body, &g)
    pos := fmt.Sprintf("[%f,%f]", g.Features[0].Geometry.Coordinates[0],
        g.Features[0].Geometry.Coordinates[1])
    return pos
}

func FormatReq(address string, lang *string, style *string, unit *string) []byte {
    req := append([]byte(`{"coordinates":`), address...)
    req = append(req, `,"language":`...)
    req = append(req, wrap(lang)...)
    req = append(req, `,"preference":`...)
    req = append(req, wrap(style)...)
    req = append(req, `,"units":`...)
    req = append(req, wrap(unit)...)
    req = append(req, "}"...)
    return req
}
