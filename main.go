package main

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "os"

    "github.com/akamensky/argparse"

    "goroute/geo"
)

var url = "https://api.openrouteservice.org/v2/directions/"

func main() {
    var set bool
    units := []string{"m", "km", "mi"}
    style := []string{"fastest", "shortest", "recommended"}

    parser := argparse.NewParser("goroute", "CLI turn-by-turn directions")
    key := parser.String("k", "key", &argparse.Options{Required: false,
        Help: "ORS api key (unnecessary with `orskey' env variable)",
        Default: ""})
    lang := parser.String("l", "language", &argparse.Options{Required: false,
        Help: "Instructions language. Two letter abbreviation", Default: "en"})
    styl := parser.Selector("s", "style", style, &argparse.Options{
        Required: false, Help: "Choice of routing style", Default: "fastest"})
    unit := parser.Selector("u", "units", units, &argparse.Options{
        Required: false, Help: "Units for distance metrics", Default: "mi"})
    walk := parser.Flag("w", "walking", &argparse.Options{Required: false,
        Help: "Specify walking rather than driving instructions."})
    pts := parser.StringList("p", "point", &argparse.Options{Required: false,
        Help: "Points or locations for which you want routes"})
    if err := parser.Parse(os.Args); err != nil {
        fmt.Print(parser.Usage(err))
        os.Exit(1)
    }
    if *key == "" {
        *key, set = os.LookupEnv("orskey")
        if !set {
            fmt.Println("Need OpenRouteService™ api key.")
            fmt.Println("Can be set on cli or through `orskey' variable.")
            os.Exit(0)
        }
    }
    if *walk {
        url = url + "foot-walking/geojson"
    } else {
        url = url + "driving-car/geojson"
    }
    if len(*pts) < 2 {
        fmt.Println("Need at least two points to provide a route")
        os.Exit(1)
    }

    geo_url := "https://api.openrouteservice.org/geocode/search?api_key=" +
    *key + "&text="

    address := `[`
    for i, point := range *pts {
        address += geo.GetPoint(point, geo_url)
        if i != len(*pts)-1 {
            address += ","
        }
    }
    address += `]`

    body := geo.FormatReq(address, lang, styl, unit)

    client := &http.Client{}
    req, _ := http.NewRequest("POST", url, bytes.NewBuffer(body))
    acceptable := "application/json, application/geo+json, " +
        "application/gpx+xml, img/png; charset=utf-8"

    req.Header.Add("Accept", acceptable)
    req.Header.Add("Authorization", *key)
    req.Header.Add("Content-Type", "application/json; charset=utf-8")

    resp, err := client.Do(req)

    if err != nil {
        fmt.Println("Error sending request to the server")
        return
    }

    defer resp.Body.Close()
    resp_body, _ := ioutil.ReadAll(resp.Body)

    var d geo.Directions
    json.Unmarshal(resp_body, &d)
    for _, v := range d.Features[0].Properties.Segments {
        fmt.Printf("\t%f %s to go, w/ eta %.2f minutes\n\n",
            v.Distance, *unit, (v.Duration / 60))
        for _, s := range v.Steps {
            fmt.Printf("%s: %s in %.3f %s\n",
                s.Name, s.Instruction, s.Distance, *unit)
        }
        fmt.Println("")
    }
}
